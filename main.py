import numpy as np
import pandas as pd
import scipy
from scipy import signal
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
import scipy.fftpack
import soundfile as sf
import xlrd

#################### Question 1 ####################
""" Prepare initial signal """
num_of_indice_in_singal = 200
t = np.linspace(start=0, stop=2, num=num_of_indice_in_singal, endpoint=False, dtype=np.float) # time and its steps
t_for_convolve = np.linspace(start=0, stop=3.99, num=2*num_of_indice_in_singal - 1, endpoint=False, dtype=np.float)
initial_pure_signal = 5 * np.sin(2*np.pi*t) # pure signal (sin, period = 2*pi rad/s)
noise = np.random.uniform(-0.5, 0.5, num_of_indice_in_singal) # uniform distributed noisy signal
initial_noisy_signal = initial_pure_signal + noise
initial_noisy_signal.reshape([1, num_of_indice_in_singal])
delta_kronecker = lambda x: 1 if x == 0 else 0 # define kroncker delta


               ##### Part 1 (1-1)#####
M1 = 0
M2 = 20
length_of_step_response = M1 + M2 + 1

step_response_of_the_MA = np.concatenate(((1/length_of_step_response) * np.ones([1, length_of_step_response], np.double)
                                          , np.zeros([1, num_of_indice_in_singal - length_of_step_response])),
                                         axis= 1)
step_response_of_the_MA.reshape(200, 1)
output_signal_for_conv_function = signal.convolve(step_response_of_the_MA.flatten(),
                                initial_noisy_signal) # use flatten function to prevent dimension error.

plt.figure(1)
plt.subplots(sharex=True)
plt.subplot(2, 1, 1)
plt.xlim([0, 2.5])
markerline, stemlines, baseline = plt.stem(t_for_convolve, np.concatenate((initial_noisy_signal,
                                                                np.zeros(num_of_indice_in_singal - 1).flatten())),
                                                                label= "Noisy Input Signal")
plt.setp(stemlines, 'linewidth', 1)
plt.legend()

plt.subplot(2, 1, 2)
plt.xlim([0, 2.5])
markerline, stemlines, baseline = plt.stem(t_for_convolve, output_signal_for_conv_function,
                                           label= "Output of the\nMA system\n(conv function)")
plt.setp(stemlines, 'linewidth', 1)
plt.xlabel("n")
plt.legend()
plt.show()
plt.savefig("./images/1-1.png")

                ##### Part 2 (1-2)#####
""" define myconv function to implement convolution function """

def myconv(x, h):
    """
    inputs: x, h which both of them are ndarray
    output: convolution of h and x signal
    description:
        to calculate convolution of two signal we should use the equation of calculating the convolution
        which is y[i] = conv(x, h) = sum( j = -inf, inf) on x[j] * h[i-j]
        so to have output signal with extended length, we oversize both input signal with zero
        to both of them have equal length. (Zero-Padding)
        as we have 2 variable (i, j) to iterate, we use nested loops. (a loop within another)
    """
    L = len(h.flatten())
    print("len x:", L)
    M = len(x.flatten())
    print('len h:', M)
    X = np.concatenate((x.flatten(), np.zeros(L).flatten())) # zero-padding
    H = np.concatenate((h.flatten(), np.zeros(M).flatten())) # zero-padding
    Y = np.zeros(L + M - 1).flatten()
    for i in range(L + M - 1):
        for j in range(M):
            if (i - j) >= 0:
                Y[i] += H[j] * X[i - j]

    return Y

# using myconv:

out_put_signal_for_myconv = myconv(step_response_of_the_MA, initial_noisy_signal)

plt.figure(2)
plt.subplots(sharex=True)
plt.subplot(2, 1, 1)
plt.xlim([0, 2.5])
markerline, stemlines, baseline = plt.stem(t_for_convolve, np.concatenate((initial_noisy_signal,
                                                                np.zeros(num_of_indice_in_singal - 1).flatten())),
                                                                label= "Noisy Input Signal")
plt.setp(stemlines, 'linewidth', 1)
plt.legend()

plt.subplot(2, 1, 2)
plt.xlim([0, 2.5])
markerline, stemlines, baseline = plt.stem(t_for_convolve, out_put_signal_for_myconv,
                                           label= "Output of the\nMA system\n(myconv function)")
plt.setp(stemlines, 'linewidth', 1)
plt.xlabel("n")
plt.legend()
plt.show()


                ##### Part3 (1-3) #####

""" using scipy.signal.lfilter (which is equivalent to filter function in MATLAB) to filter the input signal  """

implement_MA_with_Kaiser = (1/21) * np.kaiser(21, beta=0) # which is equivalent to MA with 21 sample
output_for_filter = signal.lfilter(implement_MA_with_Kaiser, [1], initial_noisy_signal)

plt.figure(3)
plt.subplots(sharex=True)
plt.subplot(2, 1, 1)
plt.xlim([0, 2.5])
markerline, stemlines, baseline = plt.stem(t_for_convolve, np.concatenate((initial_noisy_signal,
                                                                np.zeros(num_of_indice_in_singal - 1).flatten())),
                                                                label= "Noisy Input Signal")
plt.setp(stemlines, 'linewidth', 1)
plt.legend()

plt.subplot(2, 1, 2)
plt.xlim([0, 2.5])
markerline, stemlines, baseline = plt.stem(t_for_convolve, out_put_signal_for_myconv,
                                           label= "Output of the\nMA system\n(filter)")
plt.setp(stemlines, 'linewidth', 1)
plt.xlabel("n")
plt.legend()
plt.show()
plt.savefig("./images/Q1-P3.png")

                ##### Part4 (1-4) #####

def SinGen(omega, n):
    """

    :param omega: indicates frequency component
    :param n: number of samples
    :return: there are 2 output for this function:
                1) generated sinusoidal signal
                2) z transform of generated sinusoidal signal
    """
    t_for_sin_gen = np.linspace(start=0, stop=0.01 * n, num=n, endpoint=False)
    sin_sig = np.sin(omega * t_for_sin_gen)
    Z_Transform = signal.lfilter([0, np.sin(omega), 0], [1, -2*np.cos(omega), 1], [1])
    return sin_sig, Z_Transform







                ##### Part5 (1-5) #####

t_for_part_1_5 = np.linspace(0, 0.004, num=1000, endpoint=False) # num of samples = 1000
x_for_part_1_5 = np.cos(2000 * np.pi * t_for_part_1_5) + np.cos(8000 * np.pi * t_for_part_1_5) + \
                 np.cos(12000 * np.pi * t_for_part_1_5)
sampled_x_for_part_1_5 = np.cos(2000 * np.pi * np.linspace(0, 0.004, num=20, endpoint=False)) +\
                 np.cos(8000 * np.pi * np.linspace(0, 0.004, num=20, endpoint=False)) + \
                 np.cos(12000 * np.pi * np.linspace(0, 0.004, num=20, endpoint=False))


""" reconstruct original signal from sampled signal:
        Ideal filter is: sin( pi*( t - n*T ) / T) / (pi*( t - n*T ) / T) which is equal sinc(t - n*T)
        since the bandwidth of x(t) is 6 kHz, the minimum sampling rate that avoids aliasing, is 12 kHz.
        thus sampling with 5 kHz (as sampling rate) cause aliasing.
        As we have aliasing, expected reconstructed signal is 3 * cos(2000*pi*t) 
"""
reconstructed_signal = 0
for i in range(len(sampled_x_for_part_1_5)):
    reconstructed_signal += sampled_x_for_part_1_5[i] * np.sin(np.pi * (t_for_part_1_5 - i * 0.0002) / 0.0002) / \
                           (np.pi * (t_for_part_1_5 - i * 0.0002) / 0.0002)


plt.figure(4)
plt.plot(t_for_part_1_5, x_for_part_1_5, label = "x(t)", color= 'red')
plt.title("x(t) = cos(2*1000\u03C0t) + cos(8*1000\u03C0t) + cos(12*1000\u03C0t)")
plt.stem(np.linspace(0, 0.004, num=20, endpoint=False), sampled_x_for_part_1_5, label="sampled x(t)")

plt.plot(t_for_part_1_5, reconstructed_signal, label="reconstructed signal", color= 'green')
plt.xlabel("t")
plt.legend()
plt.show()

                ##### Part6 (1-6) #####

""" indicate aliasing in Frequency domain """

def get_fft_my_function(T_s):
    t_for_part_1_6 = np.linspace(start=-5, stop=5, num=int(10/T_s), endpoint=False)

    # np.seterr(divide='ignore', invalid='ignore')
    x_for_part_1_6 = np.sinc(5*t_for_part_1_6)**2

    import scipy.fftpack
    yf = scipy.fftpack.fft(x_for_part_1_6)
    freqs = 2*np.pi*scipy.fftpack.fftfreq(len(x_for_part_1_6)) / T_s
    freqs_between_minus_pi_pi = 2*np.pi*scipy.fftpack.fftfreq(len(x_for_part_1_6)) # to be in range(-pi, pi)
    return {"t" : t_for_part_1_6, "x_signal" : x_for_part_1_6,
            "dtft" : yf, "freq" : freqs, "freqs_between_minus_pi_pi" : freqs_between_minus_pi_pi}



All_T_s = [0.01, 0.25, 0.2, 0.1, 0.05]
T_s = 0.01
dic = get_fft_my_function(T_s)
for T_s in All_T_s:
    plt.figure(figsize=(20, 30))
    plt.subplot(2, 1, 1)
    plt.plot(dic["t"], dic["x_signal"], label="Time domain of\nx(t) = sinc\u00B2(5t)", color='blue')
    plt.xlim(-5, 5)
    plt.xlabel("t")
    plt.title("Time domain of given signal")
    plt.legend()

    plt.subplot(2, 1, 2)
    dic1 = get_fft_my_function(T_s)
    plt.plot(dic1["freqs_between_minus_pi_pi"], np.abs(dic1["dtft"]),
             label="DTFT of\nx[n*T] = sinc\u00B2(5*n*T)\nT = {} \n fs = {}".format(T_s, 1 / T_s), color='black')
    plt.xlim(-np.pi, np.pi)
    if T_s == 0.25:
        plt.ylim(0.75, 1.25)
    plt.legend()
    plt.title("Fourier transform of given x(t) in discrete time domain")
    plt.xlabel("\u03C9 (rad/sec)")
    plt.grid()
    plt.show()


                ##### Part7 (1-7) #####

def plot_for_each_pair_of_main_and_sampled_signal(mul, index_of_figure):
    """
    :param mul:
        as multiplier for T_s
    :param index_of_figure:
        to dedicate special figure to results for calling this function each time.
    :return:
        none
    """
    t_for_part_1_7 = np.linspace(start=-5, stop=5, endpoint=False, num=int(mul * 256))
    x = np.sinc(2 * t_for_part_1_7)
    T_s_for_1_7 = (t_for_part_1_7[1] - t_for_part_1_7[0])
    yf = scipy.fftpack.fft(x)
    freqs = 2 * np.pi * scipy.fftpack.fftfreq(len(x)) / T_s_for_1_7
    freqs_between_minus_pi_pi = 2 * np.pi * scipy.fftpack.fftfreq(len(x))  # to be in range(-pi, pi)

    dic = {"t": t_for_part_1_7, "x_signal": x,
            "dtft": yf, "freq": freqs, "freqs_between_minus_pi_pi": freqs_between_minus_pi_pi}

    plt.figure(index_of_figure)
    plt.subplot(2, 1, 1)
    plt.plot(dic["t"], dic["x_signal"], label="Time domain of\nx(t) = sinc(2t)", color='blue')
    plt.stem(t_for_part_1_7, x, 'r', label="Number of samples:\n{}".format(mul * 256))
    plt.xlim(-5, 5)
    plt.xlabel('t')
    plt.title("Time domain of given signal")
    plt.legend()

    plt.subplot(2, 1, 2)
    plt.plot(dic["freqs_between_minus_pi_pi"], np.abs(dic["dtft"]),
             label="DTFT of\nx[n*T] = sinc(2*n*T)\nT = {}".format(T_s_for_1_7), color='black')
    plt.xlim(-np.pi, np.pi)
    plt.legend()
    plt.xlabel("\u03C9 (rad/sec)")
    plt.grid()

    plt.show()

# 1/2 ratio :
plot_for_each_pair_of_main_and_sampled_signal(1/2, index_of_figure=6)
# 3 ratio :
plot_for_each_pair_of_main_and_sampled_signal(3, index_of_figure=7)
# 3/2 ratio :
plot_for_each_pair_of_main_and_sampled_signal(3/2, index_of_figure=8)

                ##### Part8 (1-8) #####

t_for_part_1_8 = np.linspace(start=0, stop=10, num=55, endpoint=False)
omega_1_8 = lambda x: 2 * np.pi * x
T_s_1_8 = t_for_part_1_8[1] - t_for_part_1_8[0]
x_for_part_1_8 = 0
for fr in [1 * np.pi / 16, 5 * np.pi / 16, 9 * np.pi / 16, 13 * np.pi / 16]:
    x_for_part_1_8 += np.cos(omega_1_8(fr) * t_for_part_1_8)

fft_1_8 = scipy.fftpack.fft(x_for_part_1_8)
freqs_for_1_8 = 2 * np.pi * scipy.fftpack.fftfreq(len(x_for_part_1_8)) / T_s_1_8
freq_minus_pi_pi_1_8 = 2 * np.pi * scipy.fftpack.fftfreq(len(x_for_part_1_8))  # to be in range(-pi, pi)

plt.figure(9)
plt.plot(freq_minus_pi_pi_1_8, np.abs(fft_1_8),
             label="DTFT of\ngiven x[n*T]\nT = {}".format(np.format_float_positional(T_s_1_8, 1)), color='black')
# plt.xlim(-np.pi, np.pi)
plt.legend()
plt.title("Q1-P8-S1")
plt.xlabel("\u03C9 (rad/sec)")
plt.grid()
plt.show()

####### second section #######

""" read xls file and diagnose the specific filter for each row of that file """
xls = pd.ExcelFile('filters.xls')
# input filters:
All_Input_Filters = np.asarray(pd.read_excel(xls, 'Sheet1', header=None))
H1 = list(All_Input_Filters[0])
H2 = list(All_Input_Filters[1])
H3 = list(All_Input_Filters[2])
H4 = list(All_Input_Filters[3])

# output filters:
All_Output_Filters = np.asarray(pd.read_excel(xls, 'Sheet2', header=None))
F1 = list(All_Output_Filters[0])
F2 = list(All_Output_Filters[1])
F3 = list(All_Output_Filters[2])
F4 = list(All_Output_Filters[3])

####### third section #######
def upsample(input_signal, factor):
    """
    :param input_signal: signal to be upsampled
    :param factor: scale for upsamoling
    :return: upsampled signal
    """
    upsampled_signal = []
    for i in range(len(input_signal)):
        upsampled_signal.append(input_signal[i])
        if not i == len(input_signal) - 1:
            for k in range(factor - 1):
                upsampled_signal.append(0.0)

    return np.asarray(upsampled_signal)

def downsample(input_signal, factor):
    """
    :param input_signal: signal to be downsampled
    :param factor: scale for downsampling
    :return: downsampled signal
    """
    downsampled_signal = []
    for i in range(len(input_signal)):
        if i % factor == 0:
            downsampled_signal.append(input_signal[i])

    return np.asarray(downsampled_signal)


plt.figure(10)
plt.plot(freq_minus_pi_pi_1_8, np.abs(fft_1_8),
             label="DTFT of\ngiven x[n*T]\nT = {}".format(np.format_float_positional(T_s_1_8, 5)), color='black')
# plt.xlim(-np.pi, np.pi)
plt.legend()
plt.title("Q1-P8-S3")
plt.xlabel("\u03C9 (rad/sec)")
plt.grid()
# input_signal_to_PU = []
for i in range(4):
    filterd_signal_input_to_PU = myconv(x_for_part_1_8, All_Input_Filters[i])
    input_signal_to_PU = downsample(filterd_signal_input_to_PU, 4)
    color = 'g'
    label = 'f = 9pi/16'
    if i == 0:
         out_put_signal_of_PU = 2 * input_signal_to_PU
         color = 'r'
         label = 'f = 1pi/16'
    elif i == 1:
        out_put_signal_of_PU = 0 * input_signal_to_PU
        color = 'b'
        label = 'f = 5pi/16'
    elif i == 2:
        out_put_signal_of_PU = input_signal_to_PU
    elif i == 3:
        out_put_signal_of_PU = 0.5 * input_signal_to_PU
        color = 'y'
        label = 'f = 13pi/16'


    upsampled_local_signal = upsample(out_put_signal_of_PU, 4)
    filtered_out_put_signal = myconv(upsampled_local_signal, All_Output_Filters[i])

    fft_of_local_signal = scipy.fftpack.fft(filtered_out_put_signal)
    freq_local_minus_pi_pi = 2 * np.pi * scipy.fftpack.fftfreq(len(fft_of_local_signal))
    plt.plot(freq_local_minus_pi_pi, np.abs(fft_of_local_signal), color, label = label)
    plt.legend()

plt.show()

########################################################################################
"""
                QUESTION 2

"""
########################################################################################

x_part_2 = np.ones(200)
x_part_2[25:50] = 0
x_part_2[75:100] = 0
x_part_2[125:150] = 0
x_part_2[175:] = 0

                ##### Part1 (2-1) #####
impulse_res_2_1 = 0.1 * np.ones(10)
y_out_2_1 = myconv(x_part_2, impulse_res_2_1)
plt.figure(11)
plt.stem([i for i in range(len(x_part_2))], x_part_2, 'b', label="given x[n]")
markerline, stemlines, baseline = plt.stem([i for i in range(len(y_out_2_1))], y_out_2_1, 'r',
                                           label="y[n] = myconv(x[n], h[n])", markerfmt ='D')
markerline.set_markerfacecolor('r')
plt.legend()
plt.title("h[n] = \u03A3 \u03B4[n - i]      0 <= i <= 9")
plt.xlabel("n")
plt.show()

                ##### Part2 (2-2) #####

impulse_res_2_2 = np.asarray([0.25 * (0.75 ** i) for i in range(15)])
y_out_2_2 = myconv(x_part_2, impulse_res_2_2)
plt.figure(12)
plt.stem([i for i in range(len(x_part_2))], x_part_2, 'b', label="given x[n]")
markerline, stemlines, baseline = plt.stem([i for i in range(len(y_out_2_2))], y_out_2_2, 'r',
                                           label="y[n] = myconv(x[n], h[n])", markerfmt ='D')
markerline.set_markerfacecolor('r')
plt.legend()
plt.title("h[n] = 0.25 * (0.75)^n     0 <= n <= 14")
plt.xlabel("n")
plt.show()

                ##### Part3 (2-3) #####
"""
    series expansion of given filter: given filter = (1/5) * (1-z^-1)^5
    series expansion : 
                    filter = 1/5 - z^-1 + 2*z^-2 - 2z^-3 + z^-4 - (1/5)*z^-5
                    z-inverse of filter:
                        h = [1/5, -1, 2, -2, 1, -1/5] 
"""
impulse_res_2_3 = np.asarray([1/5, -1, 2, -2, 1, -1/5])
y_out_2_3 = myconv(x_part_2, impulse_res_2_3)
plt.figure(13)
plt.stem([i for i in range(len(x_part_2))], x_part_2, 'b', label="given x[n]")
markerline, stemlines, baseline = plt.stem([i for i in range(len(y_out_2_3))], y_out_2_3, 'r',
                                           label="y[n] = myconv(x[n], h[n])", markerfmt ='D')
markerline.set_markerfacecolor('r')
plt.legend()
plt.title("H(z) = 1/5 * (1 - z^-1)^5")
plt.xlabel("n")
plt.show()

#######################################################
"""
        
            QUESTION 3

"""
#######################################################
omega_1_part_3 = 0.05 * np.pi
omega_2_part_3 = 0.2 * np.pi
omega_3_part_3 = 0.35 * np.pi
n_part_3 = np.linspace(start=0, stop=200, num=200, endpoint=False)
s_n_part_3 = np.sin(omega_2_part_3 * n_part_3)
v_n_part_3 = np.sin(omega_1_part_3 * n_part_3) + np.sin(omega_3_part_3 * n_part_3)
x_n_part_3 = s_n_part_3 + v_n_part_3

M = 100 # index of the given filter

n_for_filter_part_3 = np.linspace(start=0, stop=M, num=M + 1, endpoint=True)

my_sinc = lambda x: (x / np.pi) * np.sinc((x / np.pi) * (n_for_filter_part_3 - M / 2)) # where x is omega !
hamming_win_part_3 = 0.54 - 0.46 * np.cos(2 * np.pi * n_for_filter_part_3 / M)
impulse_res_part_3_1 = hamming_win_part_3 * (my_sinc(0.25 * np.pi) - my_sinc(0.15 * np.pi))

                ##### Part1 (3-1) #####
plt.figure(14)
plt.subplot(2, 1, 1)
plt.stem(n_part_3, x_n_part_3, 'k', label="x[n] = s[n] + v[n]") # stem x[n]
plt.stem(n_part_3, s_n_part_3, 'r', label="s[n] = sin[0.2 * n]") # stem s[n]
plt.legend()
plt.title("x[n] vs s[n]")
plt.xlabel("n")

plt.subplot(2, 1, 2)
y_n_part_3_1 = signal.convolve(impulse_res_part_3_1, x_n_part_3)
markerline, stemlines, baseline = plt.stem([i for i in range(len(y_n_part_3_1))], y_n_part_3_1,
                                           'k', label="y[n] = x[n] * h[n]", markerfmt ='D') # stem y[n]
markerline.set_markerfacecolor('g')
plt.stem(n_part_3, s_n_part_3, 'r', label="s[n] = sin[0.2 * n]") # stem s[n]
plt.legend()
plt.title("y[n] vs s[n]")
plt.xlabel("n")

plt.show()


                ##### Part2 (3-2) #####
"""  
    for utilization from fdatool, we use pyFDA
"""

designed_filter_coefficients = np.asarray(pd.read_csv("Designed-Filters/Q3/filter_coefficients.csv")).flatten()

plt.figure(15)
plt.subplot(2, 1, 1)
plt.stem(n_part_3, x_n_part_3, 'k', label="x[n] = s[n] + v[n]") # stem x[n]
plt.stem(n_part_3, s_n_part_3, 'r', label="s[n] = sin[0.2 * n]") # stem s[n]
plt.legend()
plt.title("x[n] vs s[n]")
plt.xlabel("n")

plt.subplot(2, 1, 2)
y_n_part_3_2 = signal.convolve(designed_filter_coefficients, x_n_part_3)
markerline, stemlines, baseline = plt.stem([i for i in range(len(y_n_part_3_2))], y_n_part_3_2,
                                           'k', label="y[n] = x[n] * h[n]", markerfmt ='D') # stem y[n]
markerline.set_markerfacecolor('g')
plt.stem(n_part_3, s_n_part_3, 'r', label="s[n] = sin[0.2 * n]") # stem s[n]
plt.legend()
plt.title("y[n] vs s[n]")
plt.xlabel("n")

plt.show()

##########################################################################
"""

                QUESTION 4

"""
##########################################################################


                ##### Part0 (4-0) #####
""" As we use given Audio01.wav so we skip this part """

                ##### Part1 (4-1) #####

data, samplerate = sf.read("Audio01.wav")

                ##### PArt2 (4-2) #####
""" as we design filter in discrete domain but given filter is in continues time domain, 
        we convert it to discrete domain by using bilinear transformation with Td = 44100
        
        * in bilinear transformation we have:
            w: freq. used in frequency response of discrete signal
            Omega: freq used in frequency response of continues signal
            
                Omega = (2/Td) * tan(w/2) 
            or:
                w = 2 * arctan(Omega * Td / 2)
        
        so we have:
            fp = 10000 (Omega_p = 10000 * 2 * pi) ........> w_p = 2 * 0.618986 = 1.237672 (freq = 0.1969816167264323)
            fs = 12000 (Omega_s = 12000 * 2 * pi) ........> w_s = 2 * 0.707306 = 1.414612 (freq = 0.2251424923571123)
"""
filter = np.asarray(pd.read_csv("Designed-Filters/Q4/coefficients_of_filter.csv")).flatten()

                ##### Part3 (4-3) #####

filtered_sound = signal.convolve(data, filter)
f_0 = 11000# center freq. of stop and pass freq. of given filter
f_s = samplerate
cos_filtered_sound = \
    2 * filtered_sound * (np.cos((2 * np.pi * f_0 / f_s) * np.asarray([i for i in range(len(filtered_sound))])))
filtered_cos_filtered_sound = signal.convolve(cos_filtered_sound, filter)
sf.write("encrypted_sound.wav", filtered_cos_filtered_sound, samplerate=samplerate)

                ##### Part4 (4-4) #####
""" retrieving the original sound """

filter_recieve_sound = signal.convolve(filtered_cos_filtered_sound, filter)
cos_filter_receive_sound = 2 * filter_recieve_sound * \
                           (np.cos((2 * np.pi * f_0 / f_s) * np.asarray([i for i in range(len(filter_recieve_sound))])))
filter_cos_filter_receive_sound = signal.convolve(cos_filter_receive_sound, filter)

sf.write("retrieved_sound.wav", filter_cos_filter_receive_sound, samplerate=samplerate)


#####################################################################################
"""

              QUESTION 5


"""
#####################################################################################

t_for_5 = np.linspace(start=0, stop=2, num=2000, endpoint=False)
T_s_5 = t_for_5[1] - t_for_5[0]
sinusoidal_signal = np.sin(200 * np.pi * t_for_5)
chirp_signal = signal.chirp(t=t_for_5, f0=400, t1= 2, f1=200, method='linear')
delta_signal = np.zeros(len(t_for_5))
delta_signal[250] = 50

main_siganl_5 = sinusoidal_signal + chirp_signal + delta_signal

                ##### Part1 (5-1) #####

Y = scipy.fftpack.fft(main_siganl_5)
freq_5 = scipy.fftpack.fftfreq(len(Y)) / T_s_5
plt.figure(16)
plt.plot(freq_5, np.abs(Y))
plt.xlabel("\u03A9 (rad/s)")
plt.ylim(0, 200)
plt.title("Fourier Transform of x[n] = Chirp + Sinusoidal + \u03B4[n-250]")
plt.show()

                ##### Part2 (5-2) #####
M = 256
n_for_filter_part_5 = np.linspace(start=0, stop=M, num= M + 1, endpoint=True)
hamming_win_part_5 = np.hamming(M)

plt.figure(30)
plt.specgram(main_siganl_5, NFFT=M, noverlap=0, Fs=1, window=hamming_win_part_5, mode='psd') #window=hamming_win_part_5)
plt.title("Hamming 256")
h = plt.ylabel("Frequency (kHz)")
h.set_rotation(90)
plt.xlabel("Time (secs)")
plt.show()

M = 512
n_for_filter_part_5 = np.linspace(start=0, stop=M, num= M + 1, endpoint=True)
hamming_win_part_5 = np.hamming(M)

plt.figure(40)
plt.specgram(main_siganl_5, NFFT=M, noverlap=0, Fs=1, window=hamming_win_part_5, mode='psd') #window=hamming_win_part_5)
plt.title("Hamming 512")
h = plt.ylabel("Frequency (kHz)")
h.set_rotation(90)
plt.xlabel("Time (secs)")
plt.show()





